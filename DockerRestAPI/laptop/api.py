# Laptop Service

from flask import Flask, redirect, url_for, request, render_template, jsonify, session, Response
from flask_restful import Resource, Api
from pymongo import MongoClient
import acp_times
import config
import csv

# Instantiate the app
app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(host="db", port=27017)
db = client.times
collection = db.controls

# The skeleton for this code comes from Geeks for Geeks
# The link for this code is https://www.geeksforgeeks.org/merge-sort/
def mergeSort(arr):
    if len(arr) > 1:
        # Finding the mid of the array
        mid = len(arr) // 2
        # Dividing the array elements
        L = arr[:mid]
        # into 2 halves
        R = arr[mid:]
        # Sorting the first half
        mergeSort(L)
        # Sorting the second half
        mergeSort(R)
        i = j = k = 0
        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i][1] < R[j][1]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1
        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1
        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1


class ListAll(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Initialize variables
        length = len(items)
        result = {}

        # Add data to the result in json format
        for i in range(length):
            items[i].pop("_id")
            key = "control{}".format(i)
            result[key] = items[i]

        return jsonify(result)


class ListAllCsv(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Establish the keys I want to grab from my database
        fields = ['distance', 'begin_date', 'begin_time', 'miles', 'km', 'location', 'open', 'close']

        # Write the data into a csv file
        with open("out.csv", "w") as csv_file:  # Python 2 version
            writer = csv.DictWriter(csv_file, fieldnames=fields, extrasaction='ignore')
            writer.writerows(items)

        # Read the csv file
        with open("out.csv", "r") as f:
            text = f.read()

        # Display the contents of the csv file
        return Response(response=render_template('contents.html', text=text))


class ListOpenOnly(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Obtain request and initialize result variable
        top = request.args.get('top')
        result = {}

        # Get the appropriate length to use
        if top is None:
            length = len(items)
        else:
            length = int(top)

        # Store data in result without close tag
        for i in range(length):
            items[i].pop("_id")
            items[i].pop("close")
            key = "control{}".format(i)
            result[key] = items[i]

        if top is None or length == 0:
            return jsonify(result)
        else:
            # Initialize variables to use
            km = []
            reordered = {}
            # Add km distances to the km list
            for key in result:
                values = (key, int(result[key]['km']))
                km.append(values)
            # Sort the km in ascending order
            mergeSort(km)
            # Reorder the results in ascending order
            for value in km:
                reordered[value[0]] = result[value[0]]
            return jsonify(reordered)


class ListOpenOnlyCsv(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Obtain request
        top = request.args.get('top')

        # Sort the data points if top is given
        if top is not None:
            kms = []
            # Only focus on the elements in the range of top
            for i in range(int(top)):
                value = (None, int(items[i]['km']))
                kms.append(value)

            # Sort the distances in ascending order
            mergeSort(kms)

            result = []
            # Add the data in sorted order
            for km in kms:
                for item in items:
                    if int(item['km']) == km[1]:
                        result.append(item)

        # Establish the keys I want to grab from my database
        fields = ['distance', 'begin_date', 'begin_time', 'miles', 'km', 'location', 'open']

        # Write the data into a csv file
        with open("out.csv", "w") as csv_file:  # Python 2 version
            writer = csv.DictWriter(csv_file, fieldnames=fields, extrasaction='ignore')
            if top is None:
                writer.writerows(items)
            else:
                writer.writerows(result)

        # Read the csv file
        with open("out.csv", "r") as f:
            text = f.read()

        # Display the contents of the csv file
        return Response(response=render_template('contents.html', text=text))


class ListCloseOnly(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Obtain request and initialize result variable
        top = request.args.get('top')
        result = {}

        # Get the appropriate length to use
        if top is None:
            length = len(items)
        else:
            length = int(top)

        # Store data in result without close tag
        for i in range(length):
            items[i].pop("_id")
            items[i].pop("open")
            key = "control{}".format(i)
            result[key] = items[i]

        if top is None or length == 0:
            return jsonify(result)
        else:
            # Initialize variables to use
            km = []
            reordered = {}
            # Add km distances to the km list
            for key in result:
                values = (key, int(result[key]['km']))
                km.append(values)
            # Sort the km in ascending order
            mergeSort(km)
            # Reorder the results in ascending order
            for value in km:
                reordered[value[0]] = result[value[0]]
            return jsonify(reordered)


class ListCloseOnlyCsv(Resource):
    def get(self):
        # Obtain data from the database
        _items = collection.find()
        items = [item for item in _items]

        # Obtain request
        top = request.args.get('top')

        # Sort the data points if top is given
        if top is not None:
            kms = []
            # Only focus on the elements in the range of top
            for i in range(int(top)):
                value = (None, int(items[i]['km']))
                kms.append(value)

            # Sort the distances in ascending order
            mergeSort(kms)

            result = []
            # Add the data in sorted order
            for km in kms:
                for item in items:
                    if int(item['km']) == km[1]:
                        result.append(item)

        # Establish the keys I want to grab from my database
        fields = ['distance', 'begin_date', 'begin_time', 'miles', 'km', 'location', 'close']


        # Write the data into a csv file
        with open("out.csv", "w") as csv_file:  # Python 2 version
            writer = csv.DictWriter(csv_file, fieldnames=fields, extrasaction='ignore')
            if top is None:
                writer.writerows(items)
            else:
                writer.writerows(result)

        # Read the csv file
        with open("out.csv", "r") as f:
            text = f.read()

        # Display the contents of the csv file
        return Response(response=render_template('contents.html', text=text))

# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll', '/listAll/json')
api.add_resource(ListAllCsv, '/listAll/csv')
api.add_resource(ListOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListOpenOnlyCsv, '/listOpenOnly/csv')
api.add_resource(ListCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListCloseOnlyCsv, '/listCloseOnly/csv')


@app.route('/')
@app.route('/index')
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')


@app.route("/new", methods=["POST"])
def new():
    """
    Add our control times/distances to the database.
    """
    # Clear the collection in the database
    collection.drop()
    # Initialize a list for the request.form data to be put in
    content = []
    app.logger.debug("request form: {}".format(request.form))
    # Loop through the request sent from the server and add data to content list
    for item in request.form:
        item_list = request.form.getlist(item)
        content.append(item_list)
    app.logger.debug("content: {}".format(content))
    # Loop through data from every row in the table on client side (20 rows)
    for i in range(20):
        # Create a dict of the items to insert into the database
        item_doc = {
            'distance': content[0][0],
            'begin_date': content[1][0],
            'begin_time': content[2][0],
            'miles': content[3][i],
            'km': content[4][i],
            'location': content[5][i],
            'open': content[6][i],
            'close': content[7][i]
        }
        # Don't insert empty controls into the database
        if item_doc['miles'] != "":
            collection.insert_one(item_doc)
    # Reset the page
    return redirect(url_for('index'))


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    # Get the arguments from the client
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('dist', 999, type=float)
    datetime = request.args.get('datetime')

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # Calculate open and close times using arguments from the client
    open_time = acp_times.open_time(km, distance, datetime)
    close_time = acp_times.close_time(km, distance, datetime)
    result = {"open": open_time, "close": close_time}
    app.logger.debug("result: {}".format(result))

    # Send data back to the client
    return jsonify(result=result)


@app.route('/display')
def display():
    _items = collection.find()
    items = [item for item in _items]
    length = len(items)
    app.logger.debug("items: {}".format(items))
    return render_template('display.html', items=items, length=length)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
