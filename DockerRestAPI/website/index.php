<html>
    <head>
        <title>Control Times</title>
    </head>

    <body>
        <h1>Control Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
            echo "<h2>Open and Close Times</h2>";
            $first = $obj->control0;
            echo "<h4>$first->distance km Brevet Starting $first->begin_date at $first->begin_time</h4>";
            foreach ($obj as $control){
              for ($i = 3; $i < 8; $i++){
                if ($i == 3){
                  echo "<p>location: $control->location</p>";
                } elseif ($i == 4){
                  echo "<p>km: $control->km</p>";
                } elseif ($i == 5){
                  echo "<p>miles: $control->miles</p>";
                } elseif ($i == 6){
                  echo "<p>open: $control->open</p>";
                } elseif ($i == 7){
                  echo "<p>close: $control->close</p>";
                }
              }
            echo "---";
          }
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
            echo "<h2>Open Times in Table Form</h2>";
            $first = $obj->control0;
            echo "<h4>$first->distance km Brevet Starting $first->begin_date at $first->begin_time</h4>";
            echo "<table>";
            echo "<tr><th>km</th><th>miles</th><th>open</th><th>location</th></tr>";
            foreach ($obj as $control){
              echo "<tr>";
              for ($i = 3; $i < 7; $i++){
                if ($i == 3){
                  echo "<td>$control->km</td>";
                } elseif ($i == 4){
                  echo "<td>$control->miles</td>";
                } elseif ($i == 5){
                  echo "<td>$control->open</td>";
                } elseif ($i == 6){
                  echo "<td>$control->location</td>";
                }
              }
              echo "</tr>";
          }
            echo "</table>";
             ?>
        </ul>
    </body>
</html>
